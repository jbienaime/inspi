# inspi #

![image from http://www.intimateweddings.com/blog/diy-ribbon-bookmarks/](http://www.dailywt.com/wp-content/uploads/2013/10/ribbon-bookmarks-diy.jpg)

Simple cli application to bookmark url.

## Use ##

```bash
  $ ./main name url description
```

## Requirements ##

The implementation of scheme used is [chicken-scheme](https://call-cc.org)

[Make](https://www.gnu.org/software/make/manual/make.html) is optional to build

Tests are done with [Misssbehave](http://wiki.call-cc.org/eggref/4/missbehave)
 egg

```bash
  $ make install
```

## Build ##

To compile the application :

```bash
  $ csc main.scm
  # or
  $ make build
```

To run tests :

```bash
  $ behave test/*
  # or
  $ make test/*
```

## Use ##

> Not yet implemented

```bash
  $ ./main ttv bobross
  # will save as
  # ((bobross http://twitch.tv/bobross twitch of bobross)
  #  ((livestream live stream related) (twitchs live stream platform))
```
