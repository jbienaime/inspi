
; file : bookmark.scm
; description : manage a bookmark item
(module bookmark (make-bookmark
                  get-url
                  get-description
                  get-name
                  update-bookmark)
  (import chicken)
  (import scheme)
  (use srfi-13)

  ; make-bookmark : string -> string -> string -> (string string string)
  (define (make-bookmark name)
   (lambda (url)
    (lambda (description)
     (list name url description))))

  ; get-name : (string string string) -> string
  (define (get-name bookmark)
    (car bookmark))

  ; get-url : (string string string) -> string
  (define (get-url bookmark)
   (cadr bookmark))

  ; get-description : (string string string) -> string
  (define (get-description bookmark)
   (caddr bookmark))

  ; update-bookmark : bookmark (string string string) :: string -> string -> bookmark -> bookmark
  (define (update-bookmark prop)
   (lambda (new-value)
    (lambda bookmark
     (let* ((bk (car bookmark))
            (url (get-url bk))
            (name (get-name bk))
            (description (get-description bk))
            (value new-value))
           (cond
             ((string= prop "name") (list value url description))
             ((string= prop "url") (list name value description))
             ((string= prop "description") (list name url value))))))))
