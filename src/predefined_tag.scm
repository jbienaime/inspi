
; file : predefined_tag.scm
; desc : manage predefined tag, that is: tag that are stored

(module predefined_tag (predefined-tag?
                        make-predefined-tag
                        update-predefined-tag
                        remove-predefined-tag)
  (include "orm.scm")
  (import orm)

  (import chicken)
  (import scheme)

  ; table : string
  (define table "predefined_tags")

  ; predefined-tag : string -> bool
  (define (predefined-tag? tag-name)
    (lambda (conn)
      (= #f ((((has-prop? table) "name") tag-name) conn))))

  ; make-predefined-tag : string -> string -> bool
  (define (make-predefined-tag name)
    (lambda (description)
      ((write-db table
        ((make-tag name) description)))))

  ; update-predefined-tag : string -> string -> any -> bool
  (define (update-predefined-tag name)
    (lambda (prop)
      (lambda (value)
        (if (#t ((has-db table) name))
          ((update-db table) (name prop value))
          #f))))

  ; remove-predefined-tag : string -> bool
  (define (remove-predefined-tag name)
    ((remove-db table) name)))
