
; file: bookmark_collection.scm
; description: manage a collection of bookmark
(module bookmark-collection (make-bookmark-collection
                             find-bookmark
                             remove-bookmark)
  (import chicken)
  (import scheme)
  (use srfi-1)
  (use srfi-13)

  ; make-bookmark-collection : bookmark -> bookmark list -> bookmark list
  (define (make-bookmark-collection bookmark)
    (lambda (bookmark-collection)
      (cons bookmark bookmark-collection)))

  ; find-bookmark : string -> bookmark list -> bookmark
  (define (find-bookmark name)
    (lambda (bookmark-collection)
      (find
        (lambda (bookmark)
          (string= (car bookmark) name))
        bookmark-collection)))

  ; remove-bookmark : string -> bookmark list -> bookmark list
  (define (remove-bookmark name)
    (lambda (bookmark-collection)
      (filter
        (lambda (bookmark)
          (not (string= (car bookmark) name)))
        bookmark-collection))))
