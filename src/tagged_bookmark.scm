
; file : tagged_bookmark.scm
; description : manage a bookmark and his tag
(module tagged-bookmark (assign-tag
                         remove-tag)

  (import chicken)
  (import scheme)
  (use srfi-1)
  (use srfi-13)
  
  ; assign-tag : bookmark -> tag -> (bookmark, tag)
  (define (assign-tag bookmark)
    (lambda (tag)
      (list bookmark tag)))

  ; remove-tag : (bookmark, tag) -> bookmark
  (define (remove-tag tagged-bookmark)
    (let ((bookmark (reverse (cdr (reverse tagged-bookmark)))))
      bookmark)))
