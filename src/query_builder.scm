
; file : query_builder.scm
; description : query builder

(module query-builder (has-prop?
                        slct
                        from
                        where=
                        ct
                        insert-into
                        remove-last-comma
                        insert-get
                        insert-get-prop
                        insert-get-value
                        insert-pair
                        write-db)


  (import chicken)
  (import scheme)
  (use srfi-1)
  (use srfi-13)
  (use data-structures)
  (use sql-de-lite)

  ; slct : string -> string
  (define (slct prop)
    (conc "SELECT " prop))

  ; from : string -> string
  (define (from table)
    (conc " FROM " table))

  ; where= : string -> string
  (define (where= prop)
    (conc " WHERE " prop "= ?"))

  ; ct : string option -> string
  (define (ct . prop)
    (conc "COUNT(" (optional prop "id") ")"))

  ; insert-into : string -> string
  (define (insert-into table)
    (conc "INSERT INTO " table " "))

  ; remove-last-comma : string list -> string list
  (define (remove-last-comma str-list)
    (let* ((last-str (car (reverse str-list)))
           (firsts-str (cdr (reverse str-list))))
      (foldr
        cons
        (list
          (list->string
            (filter
              (lambda (c)
                (not (char=? c #\,)))
              (string->list last-str))))
        firsts-str)))

  ; insert-get : string list list -> proc -> string list
  (define (insert-get pair-list)
    (lambda (lens-proc)
      (map
        (lambda (pair)
          (conc (lens-proc pair) ","))
        pair-list)))

  ; insert-get-prop : string list list -> string list
  (define (insert-get-prop pair-list)
    ((insert-get pair-list) car))

  ; insert-get-value : string list list -> string list
  (define (insert-get-value pair-list)
    ((insert-get pair-list) cadr))

  ; end : string
  (define end ";")

  ; insert-pair: string list list -> string
  (define (insert-pair pair-list)
    (conc "(" (string-join (remove-last-comma (insert-get-prop pair-list))) ")"
          " VALUES(" (string-join (remove-last-comma (insert-get-value pair-list))) ")"
          end))

  ; has-prop? : string -> string -> string -> dbHandler -> bool
  (define (has-prop? table-name)
    (lambda (prop)
      (lambda (value)
        (lambda (conn)
          (let ((q (prepare conn (conc (slct "id") (from table-name) (where= prop) end))))
            (= 1 (column-count q)))))))

  (define (write-db table-name)
    (lambda (pair-list)
      (lambda (conn)
        (let ((q (sql conn (conc (insert-into table-name) (insert-pair pair-list)))))
          (exec q))))))
