
; file : tag.scm
; description : assign a tag to a bookmark
(module tag (make-tag)

  (import chicken)
  (import scheme)

  ; make-tag : string -> tag
  (define (make-tag name)
    (lambda (description)
      (lambda (#!optional (match-list '()))
        (list name description match-list)))))
