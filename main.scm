; file : main.scm
; compiler : csc (chicken scheme compiler)

(include "src/bookmark.scm")
(include "src/bookmark_collection.scm")
(include "src/tag.scm")
(include "src/tagged_bookmark.scm")
(import bookmark)
(import bookmark-collection)
(import tag)
(import tagged_bookmark)

(define (main args)
  (let* ((url (car args))
         (name (cadr args))
         (description (caddr args))
         (bk (((make-bookmark name) url) description))
         (bk-coll ((make-bookmark-collection bk) '())))
    (display bk-coll)
    (newline)
    (display ((remove-bookmark "name") bk-coll))))


(main (command-line-arguments))
