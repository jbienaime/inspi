$CSC=csc
$TESTER=behave

install: install.sh
	bash ./install.sh

build: main.scm
	csc main.scm

test: test/*
	rm data/db.dev.sqlite
	behave test/*
	touch data/db.dev.sqlite
