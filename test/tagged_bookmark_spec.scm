
; file : tagged_bookmark_spec.scm
; description : test src/tagged_bookmark.scm
(use missbehave
  missbehave-matchers
   missbehave-stubs
   miscmacros)

(define (callee . args) 'called)
(define (call-it n)
 (repeat n (callee)))

(include "src/tag.scm")
(import tag)
(include "src/bookmark.scm")
(import bookmark)
(include "src/tagged_bookmark.scm")
(import tagged-bookmark)

(describe "smoke"
  (it "should smoke"
    (expect #t (to (be true)))))

(describe "src/tagged_bookmark"
  (describe "assign-tag"
    (it "should assign a tag to a bookmark"
      (let* ((tag (((make-tag "programming") "all programming related")))
             (bk (((make-bookmark "github") "http://github.com") "code hosting")))
          (expect ((assign-tag bk) tag)
            (be (list bk tag))))))
  (describe "remove-tag"
    (it "should remove a tag from a bookmark"
      (let* ((tag (((make-tag "programming") "all programming related")))
             (bk (((make-bookmark "github") "http://github.com") "code hosting"))
             (tag-bk ((assign-tag bk) tag)))
          (expect (remove-tag tag-bk)
            (be (list bk)))))))
