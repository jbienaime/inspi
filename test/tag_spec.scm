
; file : tag_spec.scm
; description : test src/tag.scm
(use missbehave
  missbehave-matchers
   missbehave-stubs
   miscmacros)

(define (callee . args) 'called)
(define (call-it n)
 (repeat n (callee)))

(include "src/tag.scm")
(import tag)

(describe "smoke"
  (it "should smoke"
    (expect #t (to (be true)))))

(describe "src/tag"
  (describe "make-tag"
    (it "should make a tag"
      (expect (((make-tag "programming") "all programming related"))
        (be '("programming" "all programming related" ()))))))
