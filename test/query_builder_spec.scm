
; file : query_builder_spec.scm
; description : test query_builder.scm

(use missbehave
  missbehave-matchers
   missbehave-stubs
   miscmacros)

(define (callee . args) 'called)
(define (call-it n)
 (repeat n (callee)))

(include "src/query_builder.scm")
(import query-builder)

(use sql-de-lite)

(define test-db (open-database "data/db.dev.sqlite"))
(define (prepare-test) #t
  (exec (sql test-db "DROP TABLE IF EXISTS 'test';"))
  (exec (sql test-db "CREATE TABLE test(id INTEGER PRIMARY KEY, name TEXT);"))
  (exec (sql test-db "INSERT INTO test(id, name) VALUES(1, 'test_name');"))
  (exec (sql test-db "INSERT INTO test(id, name) VALUES(2, 'test_name_two');")))

(describe "src/query_builder"
  (describe "formatting"
    (describe "slct"
      (it "should format a select"
        (expect (slct "id")
          (be "SELECT id"))))
    (describe "from"
      (it "should format a from"
        (expect (from "test")
          (be " FROM test"))))
    (describe "where="
      (it "should format a where <prop> = ?"
        (expect (where= "name")
          (be " WHERE name= ?"))))
    (describe "ct"
      (context "when prop is not set"
        (it "should count occurence of id"
          (expect (ct)
            (be "COUNT(id)"))))
      (context "when prop is set"
        (it "should count occurence of name"
          (expect (ct "name")
            (be "COUNT(name)")))))
    (describe "insert-into"
      (it "should format insert into a table"
        (expect (insert-into "test")
          (be "INSERT INTO test "))))
    (describe "remove-last-comma"
      (it "should remove last comma of a string in a string list"
        (expect (remove-last-comma (list "id," "last,"))
          (be (list "id," "last")))))
    (describe "insert-get"
      (it "should insert-get"
        (expect ((insert-get (list (list "id" 1) (list "name" "'test_one'"))) car)
          (be (list "id," "name,")))))
    (describe "insert-get-prop"
      (it "should get prop to insert"
        (expect (insert-get-prop (list (list "id" 1) (list "name" "'test_one'")))
          (be (list "id," "name,")))))
    (describe "insert-get-value"
      (it "should get value to insert"
        (expect (insert-get-value (list (list "id" 1) (list "name" "'test_name_two'")))
          (be (list "1," "'test_name_two',")))))
    (describe "insert-pair"
      (it "should format a pair to insert into a table"
        (expect (insert-pair (list (list "id" 3) (list "name" "'test_three'")))
          (be "(id, name) VALUES(3, 'test_three');")))))
  (describe "has-prop?"
    (it "should check that a value of prop"
      (prepare-test)
      (expect ((((has-prop? "test") "name") "'test_name'") test-db)
        (be #t))))
  (describe "write-db"
    (it "should insert into a table a pair list"
      (define test-db (open-database "data/db.dev.sqlite"))
      (prepare-test)
      (((write-db "test") (list (list "id" 3) (list "name" "'test_three'"))) test-db)
      (expect ((((has-prop? "test") "name") "test_two") test-db)
        (be #t))
      (close-database test-db))))
