
; file : bookmark_collection_spec.scm
; description : test src/bookmark_collection_spec

(use missbehave
  missbehave-matchers
  missbehave-stubs
  miscmacros)

(define (callee . args) 'called)
(define (call-it n)
  (repeat n (callee)))

(include "src/bookmark_collection.scm")
(import bookmark-collection)

(describe "smoke"
  (it "should smoke"
    (expect #t (to (be true)))))

(describe "src/bookmark_collection"
  (describe "make-bookmark-collection"
    (let ((bk '("github" "http://github.com" "code")))
      (it "should create a bookmark collection"
        (expect ((make-bookmark-collection bk) '())
          (be (list bk))))))
  (describe "find-bookmark"
    (let* ((bk '("github" "http://github.com" "code"))
           (bk-coll ((make-bookmark-collection bk) '())))
      (it "should find a bookmark in collection"
        (expect ((find-bookmark "github") bk-coll)
          (be bk)))))
  (describe "remove-bookmark"
    (let* ((gh-bk (list "github" "http://github.com" "code"))
           (gl-bk (list "gitlab" "http://gitlab.com" "code"))
           (bk-coll ((make-bookmark-collection gl-bk) (list gh-bk))))
        (it "should remove a bookmark"
          (expect ((remove-bookmark "github") bk-coll)
            (be ((make-bookmark-collection gl-bk) '())))))))
