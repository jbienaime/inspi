
; file : bookmark_spec.scm
; description : test src/bookmark.scm

(use missbehave
  missbehave-matchers
   missbehave-stubs
   miscmacros)

(define (callee . args) 'called)
(define (call-it n)
 (repeat n (callee)))

(include "src/bookmark.scm")
(import bookmark)

(describe "smoke"
 (it "should smoke"
  (expect #t (to (be true)))))

(describe "src/bookmark"
 (describe "make-bookmark"
  (it "should create a bookmark item"
   (expect (((make-bookmark "github") "http://github.com") "code hosting website")
    (be '("github" "http://github.com" "code hosting website")))))
 (describe "getters"
  (describe "url"
   (it "should get url of a bookmark"
    (expect (get-url '("github" "http://github.com" "code hosting website"))
     (be "http://github.com"))))
  (describe "name"
    (it "should get name of a bookmark"
      (expect (get-name '("github" "http://github.com" "code hosting website"))
       (be "github"))))
  (describe "description"
    (it "should get description of a bookmark"
      (expect (get-description '("github" "http://github.com" "code hosting website"))
       (be "code hosting website")))))
 (describe "updater"
  (describe "url"
   (it "should update url of a bookmark"
    (let ((bk (((make-bookmark "github") "http://github.com") "code")))
     (expect
       (((update-bookmark "url") "new_url") bk)
      (be (((make-bookmark "github") "new_url") "code"))))))
  (describe "name"
   (it "should update name of a bookmark"
    (let ((bk (((make-bookmark "github") "http://github.com") "code")))
     (expect
       (((update-bookmark "name") "new-name") bk)
      (be (((make-bookmark "new-name") "http://github.com") "code"))))))
  (describe "description"
   (it "should update description of a bookmark"
    (let ((bk (((make-bookmark "github") "http://github.com") "code")))
     (expect
       (((update-bookmark "description") "code hosting") bk)
      (be (((make-bookmark "github") "http://github.com") "code hosting"))))))))
